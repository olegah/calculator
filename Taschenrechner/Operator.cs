﻿namespace Taschenrechner
{
    public enum Operator
    {
        ADDITION = 1,
        SUBTRACTION,
        MULTIPLICATION,
        DIVISION,
        EXPONENTIAL,
        ROOT,
        FACTORIAL
    }
}