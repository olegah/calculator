﻿using Calculator.Calculator;
using Calculator.Funktions;

namespace Taschenrechner
{
    public class Calculator
    {
        public decimal Calculate(Operator @operator, decimal[] operands)
        {
            switch (@operator)
            {
                case Operator.ADDITION: return new Addition().AdditionFunction(operands);
                case Operator.SUBTRACTION: return new Substraction().SubtractionFunction(operands);
                case Operator.MULTIPLICATION: return new Multiplication().MultiplicationFunction(operands);
                case Operator.DIVISION: return new Division().DivisionFunction(operands);
                case Operator.EXPONENTIAL: return new Exponential().ExponentialFunction(operands);
                case Operator.ROOT: return new Root().RootFunction(operands);
                case Operator.FACTORIAL: return new Factorial().FactorialFunction(operands);
                default:
                    return -1;
            }
        }
    }
}