﻿using System.Collections.Generic;
using System.Linq;

namespace Calculator.Calculator
{
    class Addition
    {
        public decimal AdditionFunction(decimal[] operands)
        {
            return new List<decimal>(operands).Sum();
        }
    }
}
