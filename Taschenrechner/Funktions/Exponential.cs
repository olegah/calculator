﻿using System;

namespace Calculator.Funktions
{
    class Exponential
    {
        public decimal ExponentialFunction(decimal[] operands)
        {
            return Convert.ToDecimal(Math.Pow(Convert.ToDouble(operands[0]), Convert.ToDouble(operands[1])));
        }
    }
}
