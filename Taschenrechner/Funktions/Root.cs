﻿using System;

namespace Calculator.Funktions
{
    class Root
    {
        public decimal RootFunction(decimal[] operands)
        {
            return Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(operands[0])));
        }
    }
}
