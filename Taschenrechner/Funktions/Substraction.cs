﻿namespace Calculator.Funktions
{
    class Substraction
    {
        public decimal SubtractionFunction(decimal[] operands)
        {
            decimal result = 0;

            for (int i = 0; i < operands.Length; i++)
            {
                result = result - operands[i];
            }
            return result;
        }
    }
}
