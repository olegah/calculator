﻿namespace Calculator.Funktions
{
    class Factorial
    {
        public decimal FactorialFunction(decimal[] operands)
        {
            return FactorialCalculate(operands[0]);
        }
        private decimal FactorialCalculate(decimal num)
        {
            if (num <= 27) return num == 1 ? 1 : num * FactorialCalculate(num - 1);

            throw new FactorialOverflowExceprion(new Output().FactorialOverflowExceprion);
        }
    }
}
