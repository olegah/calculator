﻿namespace Calculator.Funktions
{
    class Division
    {
        public decimal DivisionFunction(decimal[] operands)
        {
            decimal result = operands[0];

            for (int i = 0; i < operands.Length; i++)
            {
                result = result / operands[i];
            }
            return result;
        }
    }
}
