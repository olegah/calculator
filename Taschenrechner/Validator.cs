﻿using System;
using System.Collections.Generic;
using System.Linq;
using Taschenrechner;

namespace Calculator
{
    class Validator
    {
        public string Value { get; set; }
        public Operator @operator { get; private set; }

        public void validateDecimal(string input)
        {
            if (decimal.TryParse(input, out decimal number))
            {
                if (string.IsNullOrEmpty(input)) throw new NumberNotValidException("Die Eingabe ist keine Zahl!");
            }
            throw new NumberNotValidException("Die Eingabe ist keine Zahl!");
        }
        public void ValidateString(string input)
        {
            if (string.IsNullOrEmpty(input)) throw new StringNotValidException("Ihre Eingabe ist falsch!");

        }
        public void ValidateOperator(string symbol)
        {
            var symbols = new List<string>() { "+", "-", "*", "/", "pow", "sqrt", "!" };

            if (!symbols.Contains(symbol)) throw new SymbolNotValidException(new Output().SymbolNotValidException2);
        }
        public void ValidateNumbers(Operator @operator, IEnumerable<string> allTheNumbers)
        {
            int numberCount = allTheNumbers.Count();

            switch (@operator)
            {
                case Operator.ADDITION:
                case Operator.SUBTRACTION:
                case Operator.MULTIPLICATION:
                case Operator.DIVISION:
                    if (numberCount < 2) throw new Exception();
                    break;
                case Operator.EXPONENTIAL:
                    if (allTheNumbers.Count() < 2 || allTheNumbers.Count() > 2) throw new ExponentialAmmountNotValidException("Die Anzahl der Exponenten ist zu klein/groß!");
                    break;
                case Operator.ROOT:
                case Operator.FACTORIAL:
                    if (numberCount > 1) throw new Exception();
                    break;
                default:
                    break;
            }
        }
    }
}