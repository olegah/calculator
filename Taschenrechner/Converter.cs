﻿using System;
using Taschenrechner;

namespace Calculator
{
    class Converter
    {
        public Operator ConvertToOperator(string symbol)
        {
            if (symbol.Contains("+")) return Operator.ADDITION;
            if (symbol.Contains("-")) return Operator.SUBTRACTION;
            if (symbol.Contains("*")) return Operator.MULTIPLICATION;
            if (symbol.Contains("/")) return Operator.DIVISION;
            if (symbol.Equals("pow", StringComparison.InvariantCultureIgnoreCase)) return Operator.EXPONENTIAL;
            if (symbol.Equals("sqrt", StringComparison.InvariantCultureIgnoreCase)) return Operator.ROOT;
            if (symbol.Contains("!")) return Operator.FACTORIAL;
            throw new SymbolNotValidException(new Output().SymbolNotValidException);
        }
    }
}
