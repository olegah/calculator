﻿using Calculator;

namespace Taschenrechner
{
    public class Node
    {
        public string Value { get; private set; }
        public Node Left { get; private set; }
        public Node Right { get; private set; }
        public Operator @operator { get; private set; }
        public Node(string value)
        {
            int bracketCount = 0;
            int pos = 0;
            int firstBracketPos = 0;

            value = value.Trim();
            Value = value;

            if (value.StartsWith('(') && value.EndsWith(')')) value = value.Substring(1, value.Length - 2);

            for (int i = 0; i < value.Length; i++)
            {
                var letter = value[i];

                if (letter == '(')
                {
                    bracketCount++;
                }
                if (letter == ')')
                {
                    bracketCount--;
                    if (bracketCount == 0)
                    {
                        pos = i;
                        break;
                    }
                }
                if (bracketCount == 1 && i < 4)
                {
                    firstBracketPos = i;

                    new Validator().ValidateOperator(value.Substring(0, firstBracketPos));
                    @operator = new Converter().ConvertToOperator(value.Substring(0, firstBracketPos));
                }
            }
            if (bracketCount == 0 && value.Length > 3)
            {
                int position = pos + 1;
                var rightValue = value.Substring(position, value.Length - position);
                var leftValue = value.Substring(2, position - firstBracketPos + 1);

                var left = new Node(leftValue);
                var right = new Node(rightValue);

                Left = left;
                Right = right;
            }
        }
    }
}