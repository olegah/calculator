﻿using Calculator;
using System;

namespace Taschenrechner
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            while (true)
            {
                Output output = new Output();
                Input input = new Input();

                Console.WriteLine(output.Choice);

                try
                {
                    Console.Write("Aufgabe: ");
                    // ( + ( * ( + 3 3 ) ( + 5 5 ) ) ( ! ( 3 ) ) )

                    //var node = new Node(Console.ReadLine());

                    input.InputCMD(Console.ReadLine());
                    //decimal Erg = new Calculator().Calculate(list.choice, list.numbers);
                    Console.WriteLine(output.Result);
                }
                catch (ChoiceNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (ExponentialAmmountNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (ExponentialNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (FactorialOverflowExceprion e)
                {
                    Console.WriteLine(e);
                }
                catch (IntNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (NumberNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (StringNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (SymbolNotValidException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                Console.WriteLine(output.WaitForKeyPress);
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}