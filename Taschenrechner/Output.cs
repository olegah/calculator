﻿namespace Calculator
{
    public class Output
    {
        /*  EXCEPTIONS  */
        public string ChoiceNotValidException => "Die Auswahl ist Falsch, bitte nur zwischen 1 und 7 wählen!";
        public string ExponentialAmmountNotValidException => "Die Anzahl der Hochzahlen ist zu groß/klein. Bitte nur 2 eingeben!";
        public string ExponentialNotValidException => "Die eingegebenen";
        public string FactorialOverflowExceprion => "\n Der eingegebene Faktorialwert ist zu groß! Bitte nur bis ! 27 nutzen.";
        public string IntNotValidException => "Die Auswahl ist Falsch, bitte nur zwischen 1 und 7 wählen!";
        public string NumberNotValidException => "Die Eingabe ist Falsch, bitte nur Zahlen eingeben!";
        public string StringNotValidException => "Die Eingabe ist Falsch, bitte nur Zahlen eingeben!";
        public string SymbolNotValidException => "Der eingegebene Operator ist Falsch!";
        public string SymbolNotValidException2 => "\n Die Auswahl ist Falsch, bitte nur: "
                                                       + "\n 1. +"
                                                       + "\n 2. -"
                                                       + "\n 3. *"
                                                       + "\n 4. /"
                                                       + "\n 5. pow"
                                                       + "\n 6. sqrt"
                                                       + "\n 7. !"
                                                       + "\n zur berechnung eingeben!";

        /*  Messages    */
        public string OverflowError => "Die eingegebenen Zahlen sind zu groß, bitte verwende kleinere Zahlen!";
        public string Choice => "1) + 2) - 3) * 4) / 5) Pow 6) Sqrt 7) ! "
            + "\n POW: 2 Zahlen zum eingeben "
            + "\n SQRT & !: 1 Zahl zum eingeben";
        public string Result => "Ergebniss: ";
        public string WaitForKeyPress => "Für eine neue Eingabe eine beliebige Taste drücken.";
    }
}
