﻿using Calculator;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Taschenrechner
{
    internal class Input
    {
        public ListOfSymbolAndDecimals InputCMD(string input)
        {
            Validator validate = new Validator();

            try
            {
                validate.ValidateString(input);
            }
            catch (StringNotValidException)
            {
                throw new StringNotValidException("Ihre Eingabe ist falsch!");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            // ( + ( * ( + 3 3 ) ( + 5 5 ) ) ( ! ( 3 ) ) )
            Node node = new Node(input);


            string[] characters = input.Split(' ');

            string symbol = characters[0];

            IEnumerable<string> allTheNumbers = characters.Skip(1);

            Operator @operator = new Operator();

            try
            {
                validate.ValidateOperator(symbol);
                @operator = new Converter().ConvertToOperator(symbol);
                validate.ValidateNumbers(@operator, allTheNumbers);
            }
            catch (StringNotValidException)
            {
                throw new StringNotValidException("Ihre Eingabe ist falsch!");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            //var list = new ListOfSymbolAndDecimals();
            //list.numbers = allTheNumbers.Select(allTheNumbers).ToArray();
            //list.choice = @operator;
            //TODO: Test
            return null;
        }
    }
}