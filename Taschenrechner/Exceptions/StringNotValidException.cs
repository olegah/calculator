﻿using System;

namespace Calculator
{
    public class StringNotValidException : Exception
    {
        public StringNotValidException(string message) : base(message) { }
    }
}
