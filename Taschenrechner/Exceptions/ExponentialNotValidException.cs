﻿using System;

namespace Calculator
{
    public class ExponentialNotValidException : Exception
    {
        public ExponentialNotValidException(string message) : base(message) { }
    }
}
