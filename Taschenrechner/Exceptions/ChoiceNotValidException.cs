﻿using System;

namespace Calculator
{
    public class ChoiceNotValidException : Exception
    {
        public ChoiceNotValidException(string message) : base(message) { }
    }
}
