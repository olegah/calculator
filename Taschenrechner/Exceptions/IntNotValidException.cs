﻿using System;

namespace Calculator
{
    public class IntNotValidException : Exception
    {
        public IntNotValidException(string message) : base(message) { }
    }
}
