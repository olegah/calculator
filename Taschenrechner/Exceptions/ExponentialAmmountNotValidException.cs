﻿using System;

namespace Calculator
{
    public class ExponentialAmmountNotValidException : Exception
    {
        public ExponentialAmmountNotValidException(string message) : base(message) { }
    }
}
