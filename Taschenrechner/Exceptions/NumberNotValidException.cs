﻿using System;

namespace Calculator
{
    public class NumberNotValidException : Exception
    {
        public NumberNotValidException(string message) : base(message) { }
    }
}
