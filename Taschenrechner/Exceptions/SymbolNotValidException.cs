﻿using System;

namespace Calculator
{
    public class SymbolNotValidException : Exception {
        public SymbolNotValidException(string message) : base(message) { }
    }
}
